-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Czas generowania: 14 Paź 2017, 15:02
-- Wersja serwera: 5.7.19
-- Wersja PHP: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `facebook`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `frends`
--

DROP TABLE IF EXISTS `frends`;
CREATE TABLE IF NOT EXISTS `frends` (
  `id_frend` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) NOT NULL COMMENT 'ID usera',
  `description` text CHARACTER SET utf8 COLLATE utf8_polish_ci COMMENT 'Tabelka',
  PRIMARY KEY (`id_frend`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id_users` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(35) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL COMMENT 'Imie użytkownika',
  `surname` varchar(35) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL COMMENT 'Nazwisko użytkownika',
  `pictures` blob NOT NULL COMMENT 'Zdjęcie użytkownika',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL COMMENT 'Unikalny adres',
  `comments` varchar(255) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL COMMENT 'Komentarz tablicy',
  `frends_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `accepted` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Pole boolean true/false',
  PRIMARY KEY (`id_users`),
  UNIQUE KEY `url` (`url`),
  KEY `fk_foreign_frends_id` (`frends_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tabela przechowuje dane użytkowników';
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
