<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Product extends Model
{
    protected $table = 'products';

    protected $fillable = [
        'product_id', 'name', 'description'
    ];

    public static function getRules()
    {
        return array(
            'name'     => 'required|regex:/^[a-zA-Z]+$/u|max:35',
            'description' => 'required|max:255'
        );
    }

    public function validate($data)
    {
        // make a new validator object
        $v = Validator::make($data, Product::getRules());
        // return the result
        return $v->passes();
    }
}
