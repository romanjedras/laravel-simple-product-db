<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Illuminate\Support\Facades\Validator;
use Session;
use App\Http\Requests\CreateProductResponse;

class ProductsController extends Controller
{

    /**
     * [[Method return all product from products]]
     */

    public function index(){

        $products = Product::latest()->get();

        return view('products.index')->with('products',$products);

    }

    public function create(){

        return view('products.create');
    }

    public function store(CreateProductResponse $request){

        $product = new Product();
        $data = [];
        $data['name'] = $product->name = $request->name;
        $data['description'] = $product->description = $request->description;

        $pass = $product->validate($data);
        if($product->save()){
            Session::flash('product_created','Product został dodany do systemu');
            return redirect('product');
        }


    }


    public function edit($id){
        $product = Product::findOrFail($id);
        return view('products.edit')->with('product',$product);
    }

    public function update($id, CreateProductResponse $request){

        $product = Product::findOrFail($id);

        $data['name'] = $product->name = $request->name;
        $data['description'] = $product->description = $request->description;
        if($product->update()){
            Session::flash('product_updated','Product został zaktualizowany');
            return redirect('product');
        }
    }

    public function destroy($id)
    {
        $product = Product::find($id);

        if(!empty($product)){
            $product->delete();
        }
        Session::flash('message_del', 'Suksesywnie usunełeś product!');
        return redirect('product');
    }


}
