@extends('layouts.master')
@section('content')
    <br/><br/><br/><br/>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="card">
                <div class="panel-body">
    {!! Form::model($product, ['method'=>'PATCH','class'=>'form-horizontal','id'=>'edit_product','action'=>['ProductsController@update', $product->id]]) !!}
                    <div class="form-group">
                        <div class="col-md-4 control-label"> {!!  Form::label('name', 'Aktualna nazwa produktu:'); !!}
                        </div>
                        <div class="col-md-6">
                            {!! Form::text('name',null,array('id'=>'name','required','class'=>'form-control','title'=>'Wybierz nazwę produktu')) !!}
                        </div>
                    </div>
                    <div class="clearfix" style="padding-bottom:6px;"></div>
                    <div class="form-group">
                        <div class="col-md-4 control-label"> {!!  Form::label('description', 'Podaj opis demo:') !!}
                        </div>
                        <div class="col-md-8">

                            <script type="text/javascript" src="{!! asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') !!}"></script>
                            <script   src="https://code.jquery.com/jquery-1.12.4.min.js"   crossorigin="anonymous"></script>
                            {!! Form::textarea('description',null,array('id'=>'description','name'=>'description')) !!}
                            <script>
                                //        CKEDITOR.replace( 'demo_description' );
                                CKEDITOR.replace('description', {
                                    filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
                                    filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token={{csrf_token()}}',
                                    filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
                                    filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token={{csrf_token()}}'
                                });
                            </script>
                        </div>
                    </div>
                    <div class="clearfix" style="padding-bottom:25px;"></div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            {!! Form::submit('Zapisz',['class'=>'btn btn-success']);!!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>