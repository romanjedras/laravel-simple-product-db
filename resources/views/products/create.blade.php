@extends('layouts.master')
@section('content')

<br/><br/>
    <h2> Dodaj nowy product</h2>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="panel-body">
                <a class="btn btn-warning btn-sm" href="{{ action('ProductsController@index')}}" rel="tooltip" title="wróć do produktów" data-toggle="tooltip" data-placement="right">Wróć do wszytkich produktów </a>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="card">
            <div class="panel-body">

                @if(count($errors) > 0)
                    <div class="allert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>Pole jest wymagane</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                    @if (Session::has('product_created'))
                        <div class="alert alert-warning card">
                            {{Session::get('product_created')}}
                        </div>
                    @endif



                    {!! Form::open(['url'=>'store_product','id'=>'create_product','method'=>'post','classes'=>'form-horizontal']) !!}

                    <div class="clearfix" style="padding-bottom:6px;"></div>
                    <div class="form-group">
                        <div class="col-md-4 control-label"> {!!  Form::label('name', 'Dodaj nowy produkt:') !!}
                        </div>
                        <div class="col-md-6">
                            {!! Form::text('name',null,array('id'=>'name','required','class'=>'form-control','title'=>'Dodaj nowy product')) !!}
                        </div>
                    </div>
                    <div class="clearfix" style="padding-bottom:6px;"></div>
                    <div class="form-group">
                        <div class="col-md-12 control-label"> {!!  Form::label('description', 'Podaj opis demo:') !!}
                        </div>
                        <div class="col-md-12">

                            <script type="text/javascript" src="{!! asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') !!}"></script>
                            <script   src="https://code.jquery.com/jquery-1.12.4.min.js"   crossorigin="anonymous"></script>
                            {!! Form::textarea('description',null,array('id'=>'description','name'=>'description')) !!}
                            <script>
                                //        CKEDITOR.replace( 'demo_description' );
                                CKEDITOR.replace('description', {
                                    filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
                                    filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token={{csrf_token()}}',
                                    filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
                                    filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token={{csrf_token()}}'
                                });
                            </script>
                        </div>
                    </div>
                    <div class="clearfix" style="padding-bottom:6px;"></div>
                    <div class="clearfix" style="padding-bottom:25px;"></div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            {!! Form::submit('Dodaj product',['class'=>'btn btn-success'])!!}

                        </div>
                    </div>
                    {!! Form::close() !!}
            </div>