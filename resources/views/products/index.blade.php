@extends('layouts.master')
@section('content')

<br/><br/>
<h2>Lista wszytkich ustawionych produktów</h2>

@if (Session::has('product_updated'))
    <div class="alert alert-warning card">
        {{Session::get('product_updated')}}
    </div>
@endif
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="card">
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Lp.</th>
                            <th>Name</th>
                            <th>Opis</th>
                            <th>Edytuj</th>
                            <th>Usuń</th>
                        </tr>
                        </thead>
                    <tbody>
                    <tr>
                        <div style="margin-bottom:20px">
                    <a class="btn btn-danger btn-sm" href="product/create">Dodaj nowy product </a>
                        </div>
                    </tr>
                    </tr>
                    <?php $i=1;?>
                    @foreach($products as $product)
                        <tr>
                            <th scope="row"><?php echo $i;?></th>
                            <td>
                            {{$product->name}}
                            </td>
                            <td>
                                {{strip_tags($product->description)}}
                            </td>
                            <td>
                                <a href="{{ action('ProductsController@edit',$product->id)}}" class="btn btn-success btn-sm">
                                    Edytuj product
                                </a>
                            </td>
                            <td>
                                {{ Form::open(array('url' => 'product/' . $product->id, 'class' => 'pull-right','id'=>'delete')) }}
                                {{ Form::hidden('_method', 'DELETE') }}
                                {{ Form::submit('Usuń', array('class' => 'btn btn-warning')) }}
                                {{ Form::close() }}
                            </td>
                            <?php $i++;?>
                        </tr>
                    @endforeach
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
@stop
        <script   src="https://code.jquery.com/jquery-1.12.4.min.js"   crossorigin="anonymous"></script>
        <script>

            $('document').ready(function() {

                $("#delete").on("click", function() {
                    return confirm("Czy chcesz usunąć ten product?");
                });

            });
        </script>